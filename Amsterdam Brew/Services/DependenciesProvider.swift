//
//  DependenciesProvider.swift
//  Amsterdam Brew
//
//  Created by Miłosz Filimowski on 16/09/2017.
//  Copyright © 2017 Miłosz Filimowski. All rights reserved.
//

import Foundation
import CoreData

protocol DependenciesProvider {
    
    var apiClient: APIClient { get }
    var peristentContainer: NSPersistentContainer { get }
}

final class DefaultDependenciesProvider: DependenciesProvider {
    
    private(set) lazy var apiClient: APIClient = {
        return DefaultAPIClient()
    }()
    
    private(set) lazy var peristentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Amsterdam_Brew")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in 
            if let error = error as NSError? {
                fatalError("Failed to load peristent stores. \(error)")
            }
        })
        return container
    }()
    
}
