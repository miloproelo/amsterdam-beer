//
//  Beer+CoreDataClass.swift
//  Amsterdam Brew
//
//  Created by Miłosz Filimowski on 16/09/2017.
//  Copyright © 2017 Miłosz Filimowski. All rights reserved.
//

import Foundation
import CoreData

@objc(Beer)
public class Beer: NSManagedObject {
    
    func update(withDictionary value: [String : Any?]) throws {
        guard let idValue = value["id"] else {
            throw JSONDeserializationError.missingDictionaryKey(key: "id")
        }
        guard let id = idValue as? String else {
            throw JSONDeserializationError.unexpectedValueType(actual: type(of: idValue), expected: String.self)
        }
        self.identifier = id
        
        if let name = value["name"] as? String {
            self.name = name
        }
        
        if let labels = value["labels"] as? [String: String?], let label = labels["medium"] {
            self.label = label
        }
        
        if let style = value["style"] as? [String: Any?], let styleName = style["name"] as? String {
                self.styleName = styleName
        }
        
        if let description = value["description"] as? String {
            self.beerDescription = description
        }
    }
    
}
