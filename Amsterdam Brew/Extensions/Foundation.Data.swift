//
//  Foundation.Data.swift
//  Amsterdam Brew
//
//  Created by Miłosz Filimowski on 16/09/2017.
//  Copyright © 2017 Miłosz Filimowski. All rights reserved.
//

import Foundation

extension Data {
    
    func toJSONDictionary() throws -> [String: Any?] {
        let object = try JSONSerialization.jsonObject(with: self, options: [])
        guard let body = object as? [String: Any?] else {
            throw JSONDeserializationError.unexpectedValueType(actual: type(of: object), expected: Dictionary<String, String?>.self)
        }
        return body
    }
}
