//
//  Foundation.URLRequest.swift
//  Amsterdam Brew
//
//  Created by Miłosz Filimowski on 15/09/2017.
//  Copyright © 2017 Miłosz Filimowski. All rights reserved.
//

import Foundation

enum APIRequestError: Error {
    case incorrectURL(url: String)
}

extension URLRequest {
    
    init(request: APIRequest) throws {
        guard let url = try URLComponents(request: request).url else {
            throw APIRequestError.incorrectURL(url: request.path)
        }
        self.init(url: url)
        
        httpMethod = request.method.rawValue
        allHTTPHeaderFields = [
            "Accept": "application/json",
            "Content-Type": "application/json; charset=utf-8",
        ]
    }
}
