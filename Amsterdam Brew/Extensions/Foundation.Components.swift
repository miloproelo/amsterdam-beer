//
//  Foundation.Components.swift
//  Amsterdam Brew
//
//  Created by Miłosz Filimowski on 15/09/2017.
//  Copyright © 2017 Miłosz Filimowski. All rights reserved.
//

import Foundation

extension URLComponents {
    
    init(request: APIRequest) throws {
        self.init()
        self.scheme = request.scheme.rawValue
        self.host = request.host
        
        self.path = "/" + request.path
        
        self.queryItems = request.query.map {
            URLQueryItem(name: $0, value: $1)
        }            
    }
    
}
