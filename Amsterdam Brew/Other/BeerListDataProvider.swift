//
//  BeerListDataProvider.swift
//  Amsterdam Brew
//
//  Created by Miłosz Filimowski on 16/09/2017.
//  Copyright © 2017 Miłosz Filimowski. All rights reserved.
//

import Foundation
import CoreData

final class BeerListDataProvider {
    
    fileprivate let apiClient: APIClient
    fileprivate let managedObjectContext: NSManagedObjectContext
    
    fileprivate(set) lazy var fetchedResultsController: NSFetchedResultsController<Beer> = { [unowned self] in
        let fetchRequest: NSFetchRequest<Beer>  = Beer.fetchRequest()
        let nameSortDescriptor = NSSortDescriptor(key: "name", ascending: true, selector: #selector(NSString.localizedStandardCompare(_:)))
        fetchRequest.sortDescriptors = [nameSortDescriptor]
        return NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
    }()
    
    var onError: ((Error) -> Void)?
    
    init(apiClient: APIClient, managedObjectContext: NSManagedObjectContext) {
        self.apiClient = apiClient
        self.managedObjectContext = managedObjectContext
    }
    
    func reloadData() {
        self.apiClient.send(request: BeerListRequest(), { [weak self] response in
            guard let `self` = self else { return }
            switch response {
            case .error(let error):
                self.onError?(error)
            case .success(let data, _):
                DispatchQueue.main.async {
                    do {
                        if let beers = try data?.toJSONDictionary()["data"] as? Array<Dictionary<String, Any?>> {
                            try beers.forEach {
                                if let identifier = $0["id"] as? String {
                                    let beer = try self.findOrCreate(inContext: self.managedObjectContext, identifier: identifier)
                                    try beer.update(withDictionary: $0)
                                }
                            }
                            try self.managedObjectContext.save()
                            
                        }
                    } catch let error {
                        self.onError?(error)
                    }
                }
            }
        })
    }
    
    func updateIfNeeded() {
        if managedObjectContext.hasChanges {
            do {
               try managedObjectContext.save() 
            } catch let error {
                onError?(error)
            }
        }
    }
    
    fileprivate func findOrCreate(inContext context: NSManagedObjectContext, identifier: String) throws -> Beer {
        let request: NSFetchRequest<Beer> = Beer.fetchRequest()
        let predicate = NSPredicate(format: "%K LIKE %@", "identifier", identifier)
        request.predicate = predicate
        
        if let result = try context.fetch(request).first {
            return result
        }
        return Beer(context: context)
    }
    
}
