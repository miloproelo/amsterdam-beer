//
//  JSONError.swift
//  Amsterdam Brew
//
//  Created by Miłosz Filimowski on 16/09/2017.
//  Copyright © 2017 Miłosz Filimowski. All rights reserved.
//

import Foundation

enum JSONDeserializationError: Error {
    case unexpectedValueType(actual: Any.Type, expected: Any.Type)
    case missingDictionaryKey(key: String)
}
