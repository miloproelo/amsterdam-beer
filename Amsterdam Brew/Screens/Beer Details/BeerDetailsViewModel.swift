//
//  BeerDetailsViewModel.swift
//  Amsterdam Brew
//
//  Created by Miłosz Filimowski on 17/09/2017.
//  Copyright © 2017 Miłosz Filimowski. All rights reserved.
//

import Foundation

struct BeerDetailsViewModel {
    
    let beer: Beer
    
    var beerName: String {
        return beer.name ?? "(Undefined)"
    }
    
    var beerDescription: String {
        return beer.beerDescription ?? "(Undefined)"
    }
    
    var beerStyle: String {
        return beer.styleName ?? "(Undefined)"
    }
    
    var beerImageURL: URL? {
        guard let urlString = beer.label else {
            return nil
        }
        return URL(string: urlString)
    }
    
}
