//
//  BeerDetailsViewController.swift
//  Amsterdam Brew
//
//  Created by Miłosz Filimowski on 17/09/2017.
//  Copyright © 2017 Miłosz Filimowski. All rights reserved.
//

import Kingfisher
import UIKit

final class BeerDetailsViewController: UIViewController {
    
    let viewModel: BeerDetailsViewModel
    
    @IBOutlet weak var beerImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var styleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    init(beer: Beer) {
        viewModel = BeerDetailsViewModel(beer: beer)
        super.init(nibName: "BeerDetailsView", bundle: Bundle.main)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameLabel.text = viewModel.beerName
        styleLabel.text = viewModel.beerStyle
        descriptionLabel.text = viewModel.beerDescription
        beerImageView.kf.setImage(with: viewModel.beerImageURL)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
}
