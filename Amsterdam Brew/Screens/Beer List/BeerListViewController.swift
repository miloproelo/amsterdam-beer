//
//  BeerListViewController.swift
//  Amsterdam Brew
//
//  Created by Miłosz Filimowski on 15/09/2017.
//  Copyright © 2017 Miłosz Filimowski. All rights reserved.
//

import CoreData
import UIKit

final class BeerListViewController: UIViewController {
     
     let dataProvider: BeerListDataProvider
     
     @IBOutlet weak var beersCollectionView: UICollectionView!
     @IBOutlet weak var beerCollectionViewFlowLayout: UICollectionViewFlowLayout!
     
     init(dataProvider: BeerListDataProvider) {
          self.dataProvider = dataProvider
          super.init(nibName: "BeerListView", bundle: Bundle.main) 
     }
     
     required init?(coder aDecoder: NSCoder) {
          fatalError("init(coder:) has not been implemented")
     }
     
     override func viewDidLoad() {
          super.viewDidLoad()
          
          beersCollectionView.register(UINib(nibName: "BeerListCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: "BeerListCollectionViewCell")
          beersCollectionView.dataSource = self
          beersCollectionView.delegate = self
          
          beerCollectionViewFlowLayout.estimatedItemSize = CGSize(width: 200.0, height: 300.0)
          beerCollectionViewFlowLayout.minimumLineSpacing = 16
          
          dataProvider.onError = errorHandler
          dataProvider.fetchedResultsController.delegate = self
          do {
               try dataProvider.fetchedResultsController.performFetch()
          } catch let error {
               errorHandler(error)
          }
          dataProvider.reloadData()
     }
     
     override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(animated)
          navigationController?.setNavigationBarHidden(true, animated: true)
          beersCollectionView.refreshControl = refreshControl
     }
     
     override func viewWillDisappear(_ animated: Bool) {
          super.viewWillDisappear(animated)
          dataProvider.updateIfNeeded()
     }
     
     @objc private func refreshBeers(refreshControl: UIRefreshControl) {
          dataProvider.reloadData()
     }
     
     fileprivate lazy var refreshControl: UIRefreshControl = {
          let refreshControl = UIRefreshControl()
          refreshControl.addTarget(self, action: #selector(refreshBeers(refreshControl:)), for: .valueChanged)
          return refreshControl
     }()
     
     fileprivate lazy var errorHandler: (Error) -> Void = {
          return { [weak self] error in
               let alertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
               alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil)
               )
               self?.present(alertController, animated: true, completion: nil)
          }
     }()
     
}

extension BeerListViewController: UICollectionViewDataSource {
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
          return dataProvider.fetchedResultsController.fetchedObjects?.count ?? 0
     }
     
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          guard let beer = dataProvider.fetchedResultsController.fetchedObjects?[indexPath.row] else {
               return UICollectionViewCell()
          }
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BeerListCollectionViewCell", for: indexPath) as! BeerListCollectionViewCell
          
          let viewModel = BeerListCollectionViewCellViewModel(beer: beer)
          cell.configure(with: viewModel)
          cell.onDidTapFavouriteButton = { cell in
               beer.isFavourite = !beer.isFavourite
               let viewModel = BeerListCollectionViewCellViewModel(beer: beer)
               cell.configure(with: viewModel)
          }
          return cell
     }
}

extension BeerListViewController: UICollectionViewDelegate {
     
     func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
          if let beer = dataProvider.fetchedResultsController.fetchedObjects?[indexPath.row] {
               let detailsViewController = BeerDetailsViewController(beer: beer)
               navigationController?.pushViewController(detailsViewController, animated: true)
          }
     }
}

extension BeerListViewController: NSFetchedResultsControllerDelegate {
     func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
          beersCollectionView.reloadData()
          beersCollectionView.refreshControl?.endRefreshing()
     }
}
