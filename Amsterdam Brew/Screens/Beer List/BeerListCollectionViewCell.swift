//
//  BeerListCollectionViewCell.swift
//  Amsterdam Brew
//
//  Created by Miłosz Filimowski on 17/09/2017.
//  Copyright © 2017 Miłosz Filimowski. All rights reserved.
//

import Kingfisher
import UIKit

class BeerListCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var favouriteButton: UIButton!
    @IBOutlet weak var beerImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    var onDidTapFavouriteButton:((BeerListCollectionViewCell) -> Void)?
    
    override func prepareForReuse() {
        super.prepareForReuse()
        nameLabel.text = nil
        beerImageView.image = nil
    }
    
    func configure(with viewModel: BeerListCollectionViewCellViewModel) {
        nameLabel.text = viewModel.beerName
        favouriteButton.setImage(UIImage(named: viewModel.favouriteImageName), for: .normal)
        beerImageView.kf.setImage(with: viewModel.beerImageURL)
    }
    
    @IBAction func didTapFavouriteButton(_ sender: Any) {
        onDidTapFavouriteButton?(self)
    }
    
}
