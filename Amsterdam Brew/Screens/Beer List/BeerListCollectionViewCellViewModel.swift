//
//  BeerListCollectionViewCellViewModel.swift
//  Amsterdam Brew
//
//  Created by Miłosz Filimowski on 18/09/2017.
//  Copyright © 2017 Miłosz Filimowski. All rights reserved.
//

import Foundation

struct BeerListCollectionViewCellViewModel {
    
    fileprivate let beer: Beer
    
    init(beer: Beer) {
        self.beer = beer
    }
    
    var beerName: String? {
        return beer.name
    }
    
    var favouriteImageName: String {
        return beer.isFavourite ? "filledStar" : "emptyStar"
    }
    
    var beerImageURL: URL? {
        guard let urlString = beer.label else {
            return nil
        }
        return URL(string: urlString)
    }
}
