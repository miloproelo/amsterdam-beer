//
//  AppDelegate.swift
//  Amsterdam Brew
//
//  Created by Miłosz Filimowski on 15/09/2017.
//  Copyright © 2017 Miłosz Filimowski. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    fileprivate lazy var applicationDependencies: DependenciesProvider = DefaultDependenciesProvider()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        
        let beerListViewController = BeerListViewController(dataProvider: BeerListDataProvider(apiClient: applicationDependencies.apiClient, managedObjectContext: applicationDependencies.peristentContainer.viewContext))
        let navigationController = UINavigationController(rootViewController: beerListViewController)

        window!.rootViewController = navigationController
        window!.makeKeyAndVisible()
        

        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        try? applicationDependencies.peristentContainer.viewContext.save()
    }

}
