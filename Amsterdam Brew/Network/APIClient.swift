//
//  DefaultAPIClient.swift
//  Amsterdam Brew
//
//  Created by Miłosz Filimowski on 15/09/2017.
//  Copyright © 2017 Miłosz Filimowski. All rights reserved.
//

import Foundation

protocol APIClient {
    
    func send(request: APIRequest, _ response: @escaping (APIResponse) -> Void)
}

final class DefaultAPIClient: APIClient {
    
    private let session: URLSession
    
    init(session: URLSession = .shared) {
        self.session = session
    }
    
    func send(request: APIRequest, _ response: @escaping (APIResponse) -> Void) {
        do {
            let urlRequest = try URLRequest(request: request)
            let task = self.session.dataTask(with: urlRequest) { data, urlResponse, error in 
                if let error = error {
                    return response(.error(error))
                } else if let urlResponse = urlResponse as? HTTPURLResponse {
                    if 200..<300 ~= urlResponse.statusCode {
                        return response(.success(data, urlResponse))
                    } else {
                        return response(.error(
                            DefaultAPIClientError.unexpectedStatusCode(statusCode: urlResponse.statusCode)))
                    }
                }
            }
            task.resume()
        }
        catch let error {
            return response(.error(error))
        }
    }
}

enum DefaultAPIClientError: Error {
    
    case unexpectedStatusCode(statusCode: Int)
}
