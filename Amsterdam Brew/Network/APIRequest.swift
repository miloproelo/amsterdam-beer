//
//  APIRequest.swift
//  Amsterdam Brew
//
//  Created by Miłosz Filimowski on 15/09/2017.
//  Copyright © 2017 Miłosz Filimowski. All rights reserved.
//

import Foundation

enum Method: String {
    case GET, POST
}

enum Scheme: String {
    case HTTP = "http"
    case HTTPS = "https"
}

protocol APIRequest {
    
    var method: Method { get }
    var scheme: Scheme { get }
    var host: String { get }
    var path: String { get }
    var query: [String: String?] { get }
    
}

extension APIRequest {
    
    var method: Method { return .GET }
    var scheme: Scheme { return .HTTPS }
    var query: [String: String?] { return [:] }
    
}
