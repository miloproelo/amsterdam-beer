//
//  BeerListRequest.swift
//  Amsterdam Brew
//
//  Created by Miłosz Filimowski on 16/09/2017.
//  Copyright © 2017 Miłosz Filimowski. All rights reserved.
//

import Foundation

struct BeerListRequest: APIRequest {
    
    var scheme: Scheme {
        return .HTTP
    }
    
    var host: String {
        return Keys.API.host
    }
    
    var path: String {
        return "v2/beers"
    }
    
    var query: [String : String?] {
        return [
            "styleId" : "30",
            "key" : Keys.API.accessToken
        ]
    }
}
