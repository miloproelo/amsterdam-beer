//
//  APIResponse.swift
//  Amsterdam Brew
//
//  Created by Miłosz Filimowski on 15/09/2017.
//  Copyright © 2017 Miłosz Filimowski. All rights reserved.
//

import Foundation

enum APIResponse {
    case success(Data?, HTTPURLResponse)
    case error(Error)
}
